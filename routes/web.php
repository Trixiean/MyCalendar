<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

// Home page route.
Route::get('/', function () {
    $tasks = [];
    if (Auth::check()) {
        $tasks = DB::table('tasks')
            ->where('userId', Auth::user()->id)
            ->where('date', '<', Carbon::now()->addMonth())
            ->where('date', '>', Carbon::now()->subWeek())
            ->orderBy('updated_at')
            ->get();
    }
    return view('tasks', compact('tasks'));
});

Route::get('/tasks', function () {
   return redirect('/tasks/0');
});

Route::get('/tasks/{RequestID}', function (Request $request, $RequestID = 0){
    $tasks = [];
    $title = 'All tasks:';
    if (Auth::check()) {
        switch ($RequestID) {
            case 0:
                $tasks = DB::table('tasks')
                    ->where('userId', Auth::user()->id)
                    ->orderBy('updated_at')
                    ->get();
                break;
            case 1:
                $title = 'Overdue tasks:';
                $tasks = DB::table('tasks')
                    ->where('userId', Auth::user()->id)
                    ->where('date', '<', Carbon::now())
                    ->where('isFinished', '0')
                    ->orderBy('updated_at')
                    ->get();
                break;
            case 2:
                $title = "Completed tasks:";
                $tasks = DB::table('tasks')
                    ->where('userId', Auth::user()->id)
                    ->where('isFinished', '1')
                    ->orderBy('updated_at')
                    ->get();
                break;
            case 3:
                $title = 'Tasks for ' . $request->date . ':';
                $tasks = DB::table('tasks')
                    ->where('userId', Auth::user()->id)
                    ->where('date', '>=',  $request->date)
                    ->where('date', '<=',  $request->date . ' 23:59:59')
                    ->orderBy('updated_at')
                    ->get();
                return view('date_tasks', [
                    'tasks' => $tasks,
                    'title' => $title
                ]);
        }
    }
    return view('only_tasks', [
        'tasks' => $tasks,
        'title' => $title
    ]);
});

Route::get('/view/{task}', function (\App\Task $task) {
    if (!$task)
        abort(404);
    if (Auth::check() && $task->userId === Auth::user()->id) {
        return view('task', [
            'task' => $task,
            'isEditMode' => false
        ]);
    }
    return view('token_exp');
});

Route::post('/view/{task}', function (\App\Task $task) {
    if (!$task)
        abort(404);
    if (Auth::check() && $task->userId === Auth::user()->id) {
        return view('task', [
            'task' => $task,
            'isEditMode' => true
        ]);
    }
    return view('token_exp');
});

// Add new task page route.
Route::post('/task', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'title' => 'required|max:255'
    ]);

    if ($validator->fails()) {
        return redirect('/')
            ->withInput()
            ->withErrors($validator);
    }

    $task = new \App\Task();
    $task->userId = Auth::user()->id;
    $task->title = $request->title;
    $task->type = 'D';
    $task->place = '-';
    $task->date = date('Y-m-d H:i:s', time());
    $task->duration = '30m';
    $task->description = '---';
    $task->isFinished = false;
    $task->created_at = date('Y-m-d H:i:s', time());
    $task->updated_at = date('Y-m-d H:i:s', time());
    $task->save();

    return redirect('/');
});

// Add new task page route.
Route::post('/dtask', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'title' => 'required|max:255',
        'place' => 'max:255',
    ]);

    if ($validator->fails()) {
        return redirect('/')
            ->withInput()
            ->withErrors($validator);
    }

    $task = new \App\Task();
    $task->userId = Auth::user()->id;
    $task->title = htmlspecialchars($request->title);
    $task->type = htmlspecialchars($request->type);
    if ($request->place)
        $task->place = htmlspecialchars($request->place);
    else
        $task->place = 'Undefined';

    $d = DateTime::createFromFormat('Y-m-d H:i:s', ($request->date . ' ' . $request->time . ':00'));
    if ($d) {
        $task->date = htmlspecialchars($request->date . ' ' . $request->time);
    } else {
        $task->date = date('Y-m-d H:i:s', time());
    }
    $task->duration = htmlspecialchars($request->duration);
    if ($request->description)
        $task->description = htmlspecialchars($request->description);
    else
        $task->description = '';
    $task->isFinished = false;
    $task->created_at = date('Y-m-d H:i:s', time());
    $task->updated_at = date('Y-m-d H:i:s', time());
    $task->save();

    return redirect('/');
});

// Save
Route::post('/task/save/{taskId}', function (Request $request, $taskId) {
    $task = (new \App\Task)->find($taskId);
    $task->title = htmlspecialchars($request->title);
    $task->type = htmlspecialchars($request->type);
    if ($request->place)
        $task->place = htmlspecialchars($request->place);
    else
        $task->place = 'Undefined';
    $d = DateTime::createFromFormat('Y-m-d H:i:s', ($request->date . ' ' . $request->time . ':00'));
    if ($d) {
        $task->date = htmlspecialchars($request->date . ' ' . $request->time);
    } else {
        $task->date = date('Y-m-d H:i:s', time());
    }
    $task->duration = htmlspecialchars($request->duration);
    if ($request->description)
        $task->description = htmlspecialchars($request->description);
    else
        $task->description = '';
    $task->updated_at = date('Y-m-d H:i:s', time());
    $task->save();

    return redirect('/view/' . $taskId);
});

// Edit
Route::post('/change/{taskId}', function(Request $request, $taskId) {
    $task = (new \App\Task)->find($taskId);
    $task->isFinished = $request->isFinished;
    $task->save();

    return redirect('/view/' . $taskId);
});

// Delete task page route.
Route::delete('/task/delete/{task}', function(\App\Task $task) {
    $task->delete();

    return redirect('/');
});

Auth::routes();


@if(count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Something went wrong!</strong>

        <ul>
            @foreach($errors->all() as $err)
                <li>{{$err}}</li>
            @endforeach
        </ul>
    </div>
@endif
@extends('layouts.app')

@section('content')

    <div class="container">
        @include('errors')

        @if(Auth::check())

            <div class="panel panel-default">
                @if(count($tasks) > 0)
                <div class="panel-heading">
                    <div class="row table-container">
                        <div class="col-sm-6 col-table-cell">
                            <h4>{{$title}}</h4>
                        </div>
                        <div class="col-sm-6 text-right col-table-cell">
                            <a href="/">Current</a> |
                            <a href="{{url('/tasks/1')}}">Overdue</a> |
                            <a href="{{url('/tasks/2')}}">Completed</a> |
                            <a href="{{url('/tasks/3')}}">Date...</a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                @if($task->isFinished)
                                    <td class="col-sm-11 bg-gray">
                                @else
                                    <td class="col-sm-11">
                                @endif
                                    <div>{{$task->title}}</div>
                                </td>
                                    @if($task->isFinished)
                                        <td class="col-sm-1 bg-gray">
                                    @else
                                        <td class="col-sm-1">
                                            @endif
                                    <form action="{{url('view/' . $task->id)}}" method="GET">
                                        <button class="btn btn-info">
                                            More
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                    <div class="panel-heading">
                        <h4>{{$title}}</h4>
                    </div>
                    <div class="panel-body">You have not any tasks yet. <a href="/tasks/0">Show all tasks.</a></div>
                @endif
            </div>

        @else

        <div class="alert alert-info">
            <a href="/login">You must be logged in.</a>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Advertisements:</h4>
            </div>
            <div class="panel-body">
                <h3>Case Opener 2018</h3>
                <div class="row">
                    <div class="col-sm-12 no-padding">
                        <img src="{{asset('images/BannerVKCO18.png')}}" class="img-responsive"/>
                    </div>
                </div>
                <div class="row top10">
                    <div class="col-sm-12">
                        Do you want to try your luck by opening real cases in CS: GO? Can you first test your luck in the simulator? Case Opener 2018 is the logical continuation of Case Opener 2017.

                        Here you will not spend anything! In addition, you have the opportunity to find out if your luck is enough to knock out a knife?
                    </div>
                </div>
                <a href="https://play.google.com/store/apps/details?id=com.nikkorejz.co18">Download for Android</a>

                <h3>Forlabs Schedule</h3>
                <div class="row">
                    <div class="col-sm-12 no-padding">
                        <img src="{{asset('images/fsima.png')}}" class="img-responsive"/>
                    </div>
                </div>
                <div class="row top10">
                    <div class="col-sm-12">
                        With this application, you can watch the schedule of your group without authorization. First select your group, and then the application will remember your group, and will show the schedule for today even without an Internet connection!

                        Actual only for students of the Faculty of Service and Advertising of Irkutsk State University
                    </div>
                </div>
                <a href="https://play.google.com/store/apps/details?id=ru.trixiean.forlabsschedule">Download for Android</a>
            </div>
        </div>

        @endif
    </div>

@endsection